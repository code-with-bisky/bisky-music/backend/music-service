package com.codewithbisky.music.model;

public enum SongType {
    AUDIO,
    VIDEO,
    PODCAST_AUDIO,
    PODCAST_VIDEO,
}
