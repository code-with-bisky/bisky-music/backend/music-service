package com.codewithbisky.music.api;

import com.codewithbisky.music.dto.SongRecord;
import com.codewithbisky.music.dto.UserRegistrationRequestRecord;
import com.codewithbisky.music.service.SongService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/songs")
@RequiredArgsConstructor
public class SongApi {

    private  final SongService songService;

    @PostMapping("/artist/{artistId}")
    public ResponseEntity<SongRecord> createNewSong(@RequestBody SongRecord requestRecord,@PathVariable String artistId){

        songService.create(requestRecord,artistId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{songId}/user/{userId}/like")
    public  void userLikeASong(@PathVariable String songId,@PathVariable String userId){
        songService.userLikeASong(songId,userId);
    }


    @PutMapping("/{songId}/user/{userId}/dislike")
    public  void userDisLikeASong(@PathVariable String songId,@PathVariable String userId){
        songService.userDisLikeASong(songId,userId);
    }


}
