package com.codewithbisky.music.dto;

import com.codewithbisky.music.model.SongType;
import com.codewithbisky.music.model.StorageType;

import java.time.LocalDateTime;

public record SongRecord (String title,
                          String storageId,
                          StorageType storageType,
                          SongType songType,
                          String albumId,
                          String genreId,
                          long duration,
                          LocalDateTime releasedDate,
                          Integer releaseYear) {
}
