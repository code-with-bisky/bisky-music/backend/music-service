package com.codewithbisky.music.service;

import com.codewithbisky.music.model.Playlist;

public interface PlaylistService {

    Playlist create(Playlist playlist, String userId);
    void deleteById(String id);
    void addSongIntoPlaylist(String playlistId, String songId);
    void removeSongFromPlaylist(String playlistId, String songId);

}
