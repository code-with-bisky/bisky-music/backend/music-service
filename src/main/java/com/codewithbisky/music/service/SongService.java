package com.codewithbisky.music.service;

import com.codewithbisky.music.dto.SongRecord;
import com.codewithbisky.music.model.Song;

public interface SongService {

    Song create(SongRecord songRecord,String artistId);
    void deleteById(String id);

    void userLikeASong(String songId,String userId);
    void userDisLikeASong(String songId,String userId);

}
