package com.codewithbisky.music.service;

import com.codewithbisky.music.model.Genre;

public interface GenreService {
    Genre create(Genre genre);
    void addArtistToGenre(String genreId, String artistId);
    void removeArtistFromGenre(String genreId, String artistId);
    void addSongToGenre(String genreId, String songId);
    void removeSongFromGenre(String genreId, String songId);
}
